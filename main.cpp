﻿#include <iostream>
#include <string>

class Player {
public:
    std::string name;
    int score;

    // Задаёт параметры по умолчанию, при отсутствии данных, иначе вносит данные
    Player() : name(""), score(0) {}

    Player(const std::string& name, int score) : name(name), score(score) {}
};

// Позволяет сортировке пузырьком производить обмен
void swapPlayers(Player& a, Player& b) {
    Player temp = a;
    a = b;
    b = temp;
}

//Получает данные ввода о количестве игроков
int main() {
    int numPlayers;
    std::cout << "Введите количество игроков: ";
    std::cin >> numPlayers;

    // Создаёт массив игроков
    Player* players = new Player[numPlayers];

    // Получает данные ввода информации о игроках (имя, очки)
    for (int i = 0; i < numPlayers; ++i) {
        std::string name;
        int score;
        std::cout << "Введите имя игрока " << i + 1 << ": ";
        std::cin >> name;
        std::cout << "Введите количество очков игрока " << name << ": ";
        std::cin >> score;
        players[i] = Player(name, score);
    }

    // Сортирует массив методом пузырька
    for (int i = 0; i < numPlayers - 1; ++i) {
        for (int j = 0; j < numPlayers - i - 1; ++j) {
            if (players[j].score < players[j + 1].score) {
                swapPlayers(players[j], players[j + 1]);
            }
        }
    }

    // Выводит в консоль отсортированные данные
    std::cout << "Имя\t\t\tОчки" << std::endl;
    for (int i = 0; i < numPlayers; ++i) {
        std::cout << players[i].name << "\t\t\t" << players[i].score << std::endl;
    }

    // Освобождает память, выделенную для массива игроков
    delete[] players;

    return 0;

//По-моему это слишком большой скачок сложности, по сравнению с другими заданиями. Небыло-бы у меня базовых знаний програмирования, я бы тут вообще чокнулся)))